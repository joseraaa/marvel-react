import { config } from "dotenv";
config();


//APP
export const PUERTO_APP = process.env.PORT_APP;
//BD
export const PUERTO_BD = process.env.PORT_BD;
export const HOST= process.env.HOST_BD;
export const BASEDATOS = process.env.NAME_BD;

