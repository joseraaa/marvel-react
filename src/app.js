import express from "express";
import morgan from "morgan";
import cors from "cors";
import rutasMArvel from "./Routes/SuperHeroes.routes.js";
import { PUERTO_BD, HOST, BASEDATOS } from "./config.js";
import mongoose from "mongoose";

const conexion = 'mongodb://' + HOST + ':' + PUERTO_BD + '/' + BASEDATOS;
mongoose.connect(conexion).then();
const app = express();

// Configura las opciones CORS
app.use(cors({
    methods: 'GET,POST,DELETE,PUT',
    optionsSuccessStatus: 200,
    origin: 'http://localhost:5173',
    credentials: true
  }));
  //app.options('*', cors());
app.use(morgan('dev'));
app.use(express.json());
app.use(rutasMArvel);
app.use(express.static('public'));

app.use((req, res) => {
  res.status(404).json({ 'status': false, 'errors': 'Not Found', 'estado': 'No funciona' });
});

export default app;