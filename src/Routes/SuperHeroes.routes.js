import { Router} from "express";
import { getSuperHeroes,saveSuperHeroes,updateSuperHero,deleteSuperHeroe} from "../Controllers/SuperHeroes.js";
import {subirImagen} from "../Middleware/Storage.js"

const rutas = Router();
rutas.get('/api/marvel',getSuperHeroes);
rutas.get('/api/marvel/:id',getSuperHeroes);
rutas.post('/api/marvel',subirImagen.single('imagen'),saveSuperHeroes);
rutas.put('/api/marvel/:id',subirImagen.single('imagen'),updateSuperHero);
rutas.delete('/api/marvel/:id',deleteSuperHeroe);

//rutas.get('/api/no',(req,res)=>{res.send('No jala')});

export default rutas;